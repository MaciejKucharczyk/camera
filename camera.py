from picamera import PiCamera
from time import sleep
from datetime import datetime
import os
import threading

camera = PiCamera()
today=datetime.today().strftime('%d-%h-%H:%M')

path = "DronePhotos/%s" % today

try:
    os.makedirs(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)

def TakePhotos():
    camera.resolution=(2592, 1944)
    camera.framerate=15
    camera.start_preview()
    i=0
    while True:
        i+=1
        sleep(1)
        camera.capture('/home/pi/Dron/camera/camera/%s/image%s.jpg' % (path, i))
    camera.stop_preview()

x = threading.Thread(target=TakePhotos)
x.start()

